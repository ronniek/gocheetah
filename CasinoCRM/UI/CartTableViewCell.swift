import UIKit

class CartTableViewCell: UITableViewCell {
    @IBOutlet var cartImageView: UIImageView!
    @IBOutlet var cartNameLabel: UILabel!
    @IBOutlet var cartPricePackagingLabel: UILabel!
    @IBOutlet var cartSubstitutableLabel: UILabel!
    @IBOutlet var cartSubTotalLabel: UILabel!
    @IBOutlet var cartQuantityLabel: UILabel!
}
