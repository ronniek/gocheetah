import UIKit

class CartTableViewController : UITableViewController {
    // members
    var cartWSData: CartWSData = CartWSData()
    
    override func viewDidLoad() {
        let dataAdapter = CartAdapter()
        dataAdapter.addCompletedEventListener { _ in
            // use object mapper to convert the JSON to a type-safe & name-safe data object
            self.cartWSData = dataAdapter.WSData
            
            // use the data in the code
            self.navigationItem.title = "Cart (\(self.cartWSData.ItemsCount)): \(self.cartWSData.CartTotal)"
            
            self.tableView.reloadData()
        }
        dataAdapter.addErrorEventListener { _ in
            PopupsHelper.presentMessageBox(self, dataAdapter.title, dataAdapter.text, PopupsHelper.createDefaultAlertAction("OK", { _ in
                
                
            }))
        }
        dataAdapter.execute()
    }
    
    // table view
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartWSData.order_items_information.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 103
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cartCell", for: indexPath) as! CartTableViewCell
        let item = cartWSData.order_items_information[indexPath.row]
        if item.PorductURL != "" {
            cell.cartImageView.downloadImage(from: URL(string: item.PorductURL)!)
        }
        cell.cartNameLabel.text = item.product.name
        cell.cartPricePackagingLabel.text = "\(item.product.UnitPrice) \(item.packaging_type)"
        cell.cartSubstitutableLabel.text = String(item.substitutable)
        cell.cartSubTotalLabel.text = item.SubTotal
        cell.cartQuantityLabel.text = String(item.quantity)

        return cell;
    }
}
