import UIKit

class DebugConfigurationsVC: UIViewControllerEx {
    // members
    var tableViewEx: UITableViewEx?
    let sections = TVSCollection()
    
    // class
    override func viewDidLoad() {
        if tableViewEx == nil {
            tableViewEx = UITableViewEx()
            tableViewEx?.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(tableViewEx!)
            
            tableViewEx?.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
            tableViewEx?.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
            tableViewEx?.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
            tableViewEx?.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        }
        tableViewEx?.setSections(getSections())
        
        StyleHelper.styleNavigation(self)
        StyleHelper.styleBG(self.view)
    }
    public override func toRootController() {
        dismissToRootController()
    }

    // methods
    func getSections() -> TVSCollection {
        sections.sections.removeAll()

        let section1 = TVSData("", StyleHelper.TVCHeight, 120, "")
        section1.rows.append(SwitchTVCData(-1, 0, 2, "MainMenuCustomerServiceEnabled", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 3, "MainMenuPromotionsEnabled", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 4, "SettingsCanChangeLanguage", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 5, "SettingsCanChangePassword", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 6, "PromotionCanAddCustomer", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 7, "PromotionRefreshList", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 8, "PromotionCanFinalize", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 9, "ValidateShowConfirmDLPopup", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 13, "EditAddressRequestAddress", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 14, "EditAddressCanSkip", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 15, "EditAddressValidateAddress", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 16, "AddPhoneRequestPhone", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 17, "AddPhoneCanSkip", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 18, "AddEMailRequestEMail", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 19, "AddEMailCanSkip", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 20, "CustomerCanSwipeNewCardAuto", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 21, "CustomerCustomerDetailsEnabled", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 22, "CustomerCanSwipeNewCardEnabled", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 23, "CustomerResetPinEnabled", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 24, "AddressEditEnabled", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 25, "PhoneEditEnabled", true, false))
        section1.rows.append(SwitchTVCData(-1, 0, 26, "EMailEditEnabled", true, false))
        sections.sections.append(section1)
        
        return sections
    }
}
