import Foundation
import UIKit

public class Consts {
    public static let APPLE_APP_ID = "1234567890"
    public static let APPLE_ITUNES_APP_URL = "https://itunes.apple.com/us/app/"
    
    public static let alpha = "abcdefghijklmnopqrstuvwxyz"
    public static let ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    public static let numeric = "01234567890"
    public static let alphaNumeric = alpha + ALPHA + numeric
    
    public static let tableViewSectionHeight: CGFloat = 50
    public static let tableViewCellHeight: CGFloat = 40
    
    public static let interactionTimeout: Double = 600
    
    public static let UTC_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"
    public static let YYYYMMDD_HHMMSS_FORMAT = "yyyy-MM-dd HH:mm:ss"

    public static let TEST_TOKEN = "7957e2da55e444f6b74342689efa0b70"
    
    public static let SESSION_TIMEOUT_SECONDS: TimeInterval = 5
}
