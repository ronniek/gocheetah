import Foundation
import ObjectMapper

open class BaseMappableData: BaseMappableDispatcher {
    // members
    public var id: Int
    
    // mapping
    open override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
    }
    
    // class
    public required init?(map: Map) {
        id = 0
        super.init(map: map)
    }
    
    // event dispatcher
    public let DataChangedEvent = "DataChangedEvent"
    public func addDataChangedEventListener(_ function: @escaping (EventResult) -> ()) {
        addEventListener(DataChangedEvent, function)
    }
}
