import Foundation
import UIKit

public extension UITableView {
    public func hideUnusedCells() {
        self.tableFooterView = UIView()
    }
}
