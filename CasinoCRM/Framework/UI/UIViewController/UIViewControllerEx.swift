import Foundation
import UIKit;

class UIViewControllerEx: UIViewController {
    public func toRootController() {
        PopupsHelper.presentDismissMessageBox(self, "Error", "Root controller not implemented. Contact the administrator", "OK")
    }
    
    public func popToRootController() {
        UIApplication.shared.keyWindow?.rootViewController?.presentedViewController!.dismiss(animated: true, completion: nil)
    }
    public func dismissToRootController() {
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
