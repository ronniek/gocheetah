import Foundation
import UIKit

public class PopupsHelper {
    // ActionSheet
    //    PopupsHelper.presentActionSheet(vc, "title", "message", PopupsHelper.createDefaultAlertAction("button1", { _ in
    //        print("1")
    //    }))
    //    PopupsHelper.presentActionSheet(vc, "title", "message", PopupsHelper.createDefaultAlertAction("button1", { _ in
    //        print("1")
    //    }), PopupsHelper.createDestructiveAlertAction("button2", { _ in
    //        print("22")
    //    }))
    //    PopupsHelper.presentActionSheet(vc, "title", "message", PopupsHelper.createDefaultAlertAction("button1", { _ in
    //        print("1")
    //    }), PopupsHelper.createDestructiveAlertAction("button2", { _ in
    //        print("22")
    //    }), PopupsHelper.createCancelAlertAction("button3", { _ in
    //        print("333")
    //    }))
    public static func presentActionSheet(_ vc: UIViewController, _ title: String, _ message: String, _ actions: UIAlertAction...) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        setTitle(alert, title)
        setMessage(alert, message)
        alert.view.tintColor = StyleHelper.buttonColor
        for action in actions {
            alert.addAction(action)
        }
        vc.present(alert, animated: true)
    }
    
    // MesasgeBox
    //    PopupsHelper.presentMessageBox(vc, "title", "message", PopupsHelper.createDefaultAlertAction("button1", { _ in
    //        print("reasdfasdfkajsdhflkjashd")
    //    }))
    //    PopupsHelper.presentMessageBox(vc, "title", "message", PopupsHelper.createDefaultAlertAction("button1", { _ in
    //        print("1")
    //    }), PopupsHelper.createDestructiveAlertAction("button2", { _ in
    //        print("22")
    //    }))
    //    PopupsHelper.presentMessageBox(vc, "title", "message", PopupsHelper.createDefaultAlertAction("button1", { _ in
    //        print("1")
    //    }), PopupsHelper.createDestructiveAlertAction("button2", { _ in
    //        print("22")
    //    }), PopupsHelper.createCancelAlertAction("button3", { _ in
    //        print("333")
    //    }))
    public static func presentMessageBox(_ vc: UIViewController, _ title: String, _ message: String, _ action: UIAlertAction) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        setTitle(alert, title)
        setMessage(alert, message)
        alert.view.tintColor = StyleHelper.buttonColor
        alert.addAction(action)
        vc.present(alert, animated: true)
    }
    public static func presentDismissMessageBox(_ vc: UIViewController, _ title: String, _ message: String, _ buttonText: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        setTitle(alert, title)
        
        var text = message
        text = text.lowercased().contains("object") ? "Please retry.." : text
        text = text.lowercased().contains("predicate") ? "Please retry.." : text
        text = text.lowercased().contains("reference") ? "Please retry.." : text
        text = text.lowercased().contains("null") ? "Please retry.." : text
        text = text.lowercased().contains("nil") ? "Please retry.." : text

        setMessage(alert, text)
        alert.view.tintColor = StyleHelper.buttonColor
        alert.addAction(createAlertAction(buttonText, .cancel, { _ in }))
        vc.present(alert, animated: true)
    }
    
    // Actions
    public static func createDefaultAlertAction(_ buttonText: String, _ actionHandler: @escaping (UIAlertAction) -> Void) -> UIAlertAction {
        return createAlertAction(buttonText, .default, actionHandler)
    }
    
    public static func createDestructiveAlertAction(_ buttonText: String, _ actionHandler: @escaping (UIAlertAction) -> Void) -> UIAlertAction {
        return createAlertAction(buttonText, .destructive, actionHandler)
    }
    
    public static func createCloseAlertAction(_ actionHandler: @escaping (UIAlertAction) -> Void) -> UIAlertAction {
        return createAlertAction("Close", .cancel, actionHandler)
    }
    public static func createCancelAlertAction(_ actionHandler: @escaping (UIAlertAction) -> Void) -> UIAlertAction {
        return createAlertAction("Cancel", .cancel, actionHandler)
    }
    public static func createRetryAlertAction(_ actionHandler: @escaping (UIAlertAction) -> Void) -> UIAlertAction {
        return createAlertAction("Retry", .cancel, actionHandler)
    }
    public static func createDismissAlertAction(_ buttonText: String) -> UIAlertAction {
        return createAlertAction(buttonText, .cancel, { _ in })
    }
    
    private static func createAlertAction(_ buttonText: String, _ style: UIAlertAction.Style, _ actionHandler: @escaping (UIAlertAction) -> Void) -> UIAlertAction {
        return UIAlertAction(title: buttonText, style: style, handler: actionHandler)
    }
    
    // methods
    private static func setTitle(_ alert: UIAlertController, _ string: String) {
        var mutableString = NSMutableAttributedString()
        mutableString = NSMutableAttributedString(string: string as String, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: StyleHelper.h2Size)])
        mutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: StyleHelper.popupTitleColor, range: NSRange(location:0, length: string.count))
        alert.setValue(mutableString, forKey: "attributedTitle")
    }
    private static func setMessage(_ alert: UIAlertController, _ string: String) {
        var mutableString = NSMutableAttributedString()
        mutableString = NSMutableAttributedString(string: string as String, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: StyleHelper.regularSize)])
        mutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: StyleHelper.popupMessageColor, range: NSRange(location:0, length: string.count))
        alert.setValue(mutableString, forKey: "attributedMessage")
    }
    
    private static func getMutableAttributedString(_ string: String, _ font: UIFont) -> NSMutableAttributedString {
        var mutableAttributedString = NSMutableAttributedString()
        mutableAttributedString = NSMutableAttributedString(string: string as String, attributes: [NSAttributedString.Key.font:font])
        return mutableAttributedString
    }
}
