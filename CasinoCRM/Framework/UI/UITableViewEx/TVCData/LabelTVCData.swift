import UIKit;

class LabelTVCData: BaseTVCData {
    // members
    private var label: String
    public var valueColor: UIColor?

    override var value: String {
        get {
            return label
        }
        set(value) {
            label = value
        }
    }
    
    // class
    public init(_ id: Int, _ section: Int, _ row: Int, _ title: String, _ label: String, _ valueColor: UIColor? = nil) {
        self.label = label
        if valueColor != nil {
            self.valueColor = valueColor ?? StyleHelper.getColor("FFFFFF", "181818", "1D2129")
        }
        super.init(id, section, row, TVCTypeEnum.TitleLabel, title)
    }
}
