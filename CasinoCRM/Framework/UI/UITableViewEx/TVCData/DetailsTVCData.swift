import UIKit;

class DetailsTVCData: BaseTVCData {
    // members
    private var label: String
    public var valueColor: UIColor?

    override var value: String {
        get {
            return label
        }
        set(value) {
            label = value
        }
    }
    
    // class
    public init(_ id: Int, _ section: Int, _ row: Int, _ title: String, _ label: String, _ valueColor: UIColor? = nil) {
        self.label = label
        self.valueColor = valueColor
        super.init(id, section, row, TVCTypeEnum.TitleDetails, title)
    }
}
