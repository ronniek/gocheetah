class SwitchTVCData: BaseTVCData {
    // members
    public var isOn: Bool
    public var isOnEnabled: Bool

    // class
    public init(_ id: Int, _ section: Int, _ row: Int, _ title: String, _ isOn: Bool, _ isOnEnabled: Bool) {
        self.isOn = isOn
        self.isOnEnabled = isOnEnabled
        super.init(id, section, row, TVCTypeEnum.TitleSwitch, title)
    }
}
