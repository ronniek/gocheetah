import UIKit;

class TextFieldTVCData: BaseTVCData {
    // members
    public var titleWidth: CGFloat
    private var text: String
    public var clearButton: Bool
    public var keyboardType: UIKeyboardType

    override var value: String {
        get {
            return text
        }
        set(value) {
            text = value
        }
    }
    
    // class
    public init(_ id: Int, _ section: Int, _ row: Int, _ title: String, _ titleWidth: CGFloat, _ text: String, _ clearButton: Bool, _ keyboardType: UIKeyboardType) {
        self.titleWidth = titleWidth
        self.text = text
        self.clearButton = clearButton
        self.keyboardType = keyboardType
        super.init(id, section, row, TVCTypeEnum.TitleTextField, title)
    }
}
