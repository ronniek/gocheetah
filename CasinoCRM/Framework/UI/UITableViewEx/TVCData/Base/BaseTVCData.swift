class BaseTVCData {
    // members
    var ID: Int
    var section: Int
    var row: Int
    var tableViewCellType: TVCTypeEnum
    var title: String

    var value: String {
        get {
            return ""
        }
        set(value) {
            _ = value
        }
    }
    
    // class
    public init(_ id: Int, _ section: Int, _ row: Int, _ tableViewCellType: TVCTypeEnum, _ title: String) {
        self.ID = id
        self.section = section
        self.row = row
        self.tableViewCellType = tableViewCellType
        self.title = title
    }
}
