import UIKit;

class TVSData {
    // members
    public var header: String?
    public var rows: [BaseTVCData]
    public var rowHeight: CGFloat
    public var titleWidth: CGFloat
    public var footer: String?

    var count: Int {
        return rows.count
    }
    
    // class
    public init(_ header: String?, _ rowHeight: CGFloat, _ titleWidth: CGFloat, _ footer: String?) {
        self.header = header
        self.rows = [BaseTVCData]()
        self.rowHeight = rowHeight
        self.titleWidth = titleWidth
        self.footer = footer
    }
    
    // subscript
    public subscript(index: Int) -> BaseTVCData {
        get {
            return rows[index]
        }
        set {
            rows[index] = newValue
        }
    }
}
