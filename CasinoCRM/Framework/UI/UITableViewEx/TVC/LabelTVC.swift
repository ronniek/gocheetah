import UIKit;

class LabelTVC: BaseLabelTVC {
    // members
    var valueLabel: UILabel!
    
    var getValue: String {
        return valueLabel.text ?? ""
    }
    
    // class
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
    init(_ tableViewCellData: LabelTVCData) {
        super.init("LabelTVC", tableViewCellData)
        
        valueLabel = UILabel()
        if tableViewCellData.valueColor != nil {
            valueLabel.textColor = tableViewCellData.valueColor
        }
        valueLabel.text = tableViewCellData.value
        valueLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(valueLabel)
        
        valueLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        valueLabel.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor, constant: 0).isActive = true
    }
}
