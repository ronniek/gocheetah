import UIKit;

class ButtonTVC: BaseTVC {
    // members
    var button: UIButton!
    var function: (ButtonTVCData) -> ()
    
    // class
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
    init(_ tableViewCellData: ButtonTVCData, _ function: @escaping (ButtonTVCData) -> ()) {
        self.function = function
        super.init("ButtonTVC", tableViewCellData)
        
        button = UIButton()
        button.setTitle(tableViewCellData.title, for: UIControl.State.normal)
        button.setTitleColor(tableViewCellData.buttonColor, for: UIControl.State.normal)
        button.titleLabel?.font = .systemFont(ofSize: tableViewCellData.buttonFontSize)

        button.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(button)
        
        button.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        switch (tableViewCellData.hAlignmentType) {
        case .Left:
            button.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor, constant: 0).isActive = true
        case .Center:
            button.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        case .Right:
            button.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor, constant: 8).isActive = true
        }
        
        button.addTarget(self, action: #selector(buttonClick(sender: )), for: UIControl.Event.touchUpInside)
    }
    
    // methods
    override func isEnabled(_ enabled: Bool) {
        super.isEnabled(enabled)
        button.isUserInteractionEnabled = false
        button.alpha = 0.5
    }
    
    // events
    @objc override func tvcTapped(sender: UITapGestureRecognizer) {
        function(tableViewCellData as! ButtonTVCData)
    }
    @objc func buttonClick(sender: UIButton) {
        function(tableViewCellData as! ButtonTVCData)
    }
}
