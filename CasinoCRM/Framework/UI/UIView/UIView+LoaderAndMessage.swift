import Foundation
import UIKit

public extension UIView {
    public func addSmallLoader(_ loaderColor: UIColor = .gray) {
        self.isUserInteractionEnabled = false
        addLoader(.gray, loaderColor, nil, nil)
    }
    public func addSmallLoaderAndMessage(_ loaderColor: UIColor = .gray, _ message: String = "Loading..", _ messageColor: UIColor = .black) {
        self.isUserInteractionEnabled = false
        addLoader(.gray, loaderColor, message, messageColor)
    }

    public func addLargeLoader(_ loaderColor: UIColor = .gray) {
        self.isUserInteractionEnabled = false
        addLoader(.whiteLarge, loaderColor, nil, nil)
    }
    public func addLargeLoaderAndMessage(_ loaderColor: UIColor = .gray, _ message: String = "Loading..", _ messageColor: UIColor = .black) {
        self.isUserInteractionEnabled = false
        addLoader(.whiteLarge, loaderColor, message, messageColor)
    }

    public func addMessage(_ message: String = "Loading..", _ messageColor: UIColor = .black) {
        self.isUserInteractionEnabled = false
        addLoader(nil, nil, message, messageColor)
    }

    fileprivate func addLoader(_ style: UIActivityIndicatorView.Style?, _ loaderColor: UIColor?, _ message: String?, _ messageColor: UIColor?) {
        if style != nil {
            if getLoader() == nil {
                let loader = UIActivityIndicatorView(style: style!)
                loader.color = loaderColor
                loader.hidesWhenStopped = true
                loader.translatesAutoresizingMaskIntoConstraints = false
                self.addSubview(loader)
                
                loader.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
                loader.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
                
                if message != nil {
                    let label = addLabel(message!, messageColor!)
                    
                    label.topAnchor.constraint(equalTo: loader.bottomAnchor, constant: 10).isActive = true
                    label.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
                }
                
                startLoader()
            }
        } else {
            if message != nil {
                let label = addLabel(message!, messageColor!)
                
                label.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
                label.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
            }
        }
    }
    fileprivate func addLabel(_ message: String, _ messageColor: UIColor) -> UILabel {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 0))
        label.text = message
        label.textColor = messageColor
        label.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(label)
        return label
    }
    
    public func removeLoaderAndMessage() {
        self.isUserInteractionEnabled = true
        if let loader = getLoader() {
            loader.stopAnimating()
            loader.removeFromSuperview()
        }
        if let message = getMessage() {
            message.removeFromSuperview()
        }
    }
    
    fileprivate func getLoader() -> UIActivityIndicatorView? {
        for view in self.subviews {
            if view is UIActivityIndicatorView {
                return view as? UIActivityIndicatorView
            }
        }
        return nil
    }
    fileprivate func getMessage() -> UILabel? {
        for view in self.subviews {
            if view is UILabel {
                return view as? UILabel
            }
        }
        return nil
    }
    
    public func startLoader() {
        getLoader()?.startAnimating()
    }
    public func stopLoader() {
        getLoader()?.stopAnimating()
    }

    public func blink() {
        self.alpha = 0.2
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [.curveLinear], animations: {self.alpha = 1.0}, completion: nil)
    }
}
