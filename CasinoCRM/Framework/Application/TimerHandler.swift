import Foundation

public class TimerHandler: EventDispatcher {
    // members
    private var timers: [TimeInterval:Timer]
    
    // class
    override init() {
        timers = [TimeInterval:Timer]()
        super.init()
    }
    
    // timer
    private func start(_ timeInterval: TimeInterval) {
        if timers[timeInterval] == nil {
            let timer: Timer
            
            if #available(iOS 10.0, *) {
                timer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true, block: self.onTimer)
            } else {
                timer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(self.onTimer), userInfo: nil, repeats: true)
            }
            
            timers[timeInterval] = timer
        }
    }
    private func stop(_ timenterval: TimeInterval) {
        if let timer = timers[timenterval] {
        	timer.invalidate()
            timers[timenterval] = nil
        }
    }
    
    // events
    @objc private func onTimer(timer: Timer) {
        RaiseEvent(TickEvent, nil, timer.timeInterval)
    }
    
    // event dispatcher
    private let TickEvent = "TickEvent"
    public func addTickEventListener(_ timeInterval: TimeInterval, _ function: @escaping (EventResult) -> ()) {
        addEventListener(TickEvent, function)
        start(timeInterval)
    }
    public func removeTickEventListener(_ timeInterval: TimeInterval) {
        removeEventListener(TickEvent)
        stop(timeInterval)
    }
}
