import Foundation
import UIKit

public class AppDelegateHandler: EventDispatcher {
    // consts
    private let ApplicationDidFinishLaunchingWithOptionsEvent = "ApplicationDidFinishLaunchingWithOptionsEvent"
    
    private let ApplicationWillResignActiveEvent = "ApplicationWillResignActiveEvent"
    private let ApplicationDidEnterBackgroundEvent = "ApplicationDidEnterBackgroundEvent"
    private let ApplicationWillEnterForegroundEvent = "ApplicationWillEnterForegroundEvent"
    private let ApplicationDidBecomeActiveEvent = "ApplicationDidBecomeActiveEvent"
    private let ApplicationWillTerminateEvent = "ApplicationWillTerminateEvent"
    
    // members
    open var rootViewController: UIViewController {
        return (UIApplication.shared.keyWindow?.rootViewController)!
    }
    
    // listeners
    public func addApplicationDidFinishLaunchingWithOptionsEventListener(_ function: @escaping (EventResult) -> ()) {
        addEventListener(ApplicationDidFinishLaunchingWithOptionsEvent, function)
    }
    public func removeApplicationDidFinishLaunchingWithOptionsEventListener() {
        removeEventListener(ApplicationDidFinishLaunchingWithOptionsEvent)
    }
    
    public func addApplicationWillResignActiveEventListener(_ function: @escaping (EventResult) -> ()) {
        addEventListener(ApplicationWillResignActiveEvent, function)
    }
    public func removeApplicationWillResignActiveEventListener() {
        removeEventListener(ApplicationWillResignActiveEvent)
    }

    public func addApplicationDidEnterBackgroundEventListener(_ function: @escaping (EventResult) -> ()) {
        addEventListener(ApplicationDidEnterBackgroundEvent, function)
    }
    public func removeApplicationDidEnterBackgroundEventListener() {
        removeEventListener(ApplicationDidEnterBackgroundEvent)
    }

    public func addApplicationWillEnterForegroundEventListener(_ function: @escaping (EventResult) -> ()) {
        addEventListener(ApplicationWillEnterForegroundEvent, function)
    }
    public func removeApplicationWillEnterForegroundEventListener() {
        removeEventListener(ApplicationWillEnterForegroundEvent)
    }

    public func addApplicationDidBecomeActiveEventListener(_ function: @escaping (EventResult) -> ()) {
        addEventListener(ApplicationDidBecomeActiveEvent, function)
    }
    public func removeApplicationDidBecomeActiveEventListener() {
        removeEventListener(ApplicationDidBecomeActiveEvent)
    }

    public func addApplicationWillTerminateEventListener(_ function: @escaping (EventResult) -> ()) {
        addEventListener(ApplicationWillTerminateEvent, function)
    }
    public func removeApplicationWillTerminateEventListener() {
        removeEventListener(ApplicationWillTerminateEvent)
    }

    // raise
    public func RaiseApplicationDidFinishLaunchingWithOptionsEvent() {
        RaiseEvent(ApplicationDidFinishLaunchingWithOptionsEvent, self)
    }
    
    public func RaiseApplicationWillResignActiveEvent() {
        RaiseEvent(ApplicationWillResignActiveEvent, self)
    }
    public func RaiseApplicationDidEnterBackgroundEvent() {
        RaiseEvent(ApplicationDidEnterBackgroundEvent, self)
    }
    public func RaiseApplicationWillEnterForegroundEvent() {
        RaiseEvent(ApplicationWillEnterForegroundEvent, self)
    }
    public func RaiseApplicationDidBecomeActiveEvent() {
        RaiseEvent(ApplicationDidBecomeActiveEvent, self)
    }
    public func RaiseApplicationWillTerminateEvent() {
        RaiseEvent(ApplicationWillTerminateEvent, self)
    }
}
