import Foundation

public class DataLayerHelper {
    // methods
    public static func readJsonFromPath(_ path: URL) -> String {
        do {
            //return try String(contentsOf: path, encoding: String.Encoding.unicode)
            return try String(contentsOf: path, encoding: String.Encoding.utf8)
        }
        catch {
            return "{}"
        }
    }
    public static func writeJsonToPath(_ json: String, _ path: URL) {
        do {
            //try json.write(to: path, atomically: false, encoding: String.Encoding.unicode)
            try json.write(to: path, atomically: false, encoding: String.Encoding.utf8)
        }
        catch {
        }
    }
}
