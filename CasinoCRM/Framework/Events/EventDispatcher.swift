import Foundation

open class EventDispatcher {
    // members
    private var listeners: [ListenerItem]
    public var hasListeners: Bool {
        return listeners.count > 0
    }

    // events
    public let CompletedEvent = "CompletedEvent"
    public func addCompletedEventListener(_ function: @escaping (EventResult) -> ()) {
        addEventListener(CompletedEvent, function)
    }
    public let ErrorEvent = "ErrorEvent"
    public func addErrorEventListener(_ function: @escaping (EventResult) -> ()) {
        addEventListener(ErrorEvent, function)
    }
    
    // class
    public init() {
        listeners = [ListenerItem]()
    }
    deinit {
        DisposeListeners()
    }
    public func DisposeListeners() {
        while listeners.count > 0 {
            _ = listeners.popLast()
        }
        listeners = []
    }
    
    // methods
    // ["firstName":"Ronnie", "lastName": "Kleinfeld"]
    open func toDictionary() -> [String: Any] {
        var result = [String: Any]()
        var mirror: Mirror? = Mirror(reflecting: self)
        while mirror != nil {
            for attr in (mirror?.children.enumerated())! {
                if let label = attr.element.label as String? {
                    result[label] = attr.element.value
                }
            }
            mirror = mirror?.superclassMirror
        }
        return result
    }
    // firstName=Ronnie, lastName=Kleinfeld
    open func toString() -> String {
        var result = ""
        var mirror: Mirror? = Mirror(reflecting: self)
        while mirror != nil {
            for attr in (mirror?.children.enumerated())! {
                if let label = attr.element.label as String? {
                    if result != "" {
                        result = result + ", "
                    }
                    result = result + "\(label)=\(attr.element.value)"
                }
            }
            mirror = mirror?.superclassMirror
        }
        return result
    }

    // events
    public func addEventListener(_ type: String, _ function: @escaping (EventResult) -> ()) {
        listeners.append(ListenerItem(type, function))
    }
    public func removeEventListener(_ type: String) {
        let index = listeners.index { (listenerItem) -> Bool in
            return listenerItem.type == type
        }
        if index != nil {
            listeners.remove(at: index!)
        }
    }
    
    public func RaiseEvent(_ type: String, _ sender: AnyObject?, _ value: Any? = nil) {
        let eventResult = EventResult(type, sender, value)
        //print("RaiseEvent:\(type):\(String(describing: sender)):\(String(describing: value))")
        RaiseEvent(eventResult)
    }
    public func RaiseEvent(_ type: String, _ sender: AnyObject?, _ value: Any?, _ errorDescription: String, _ errorTitle: String, _ errorMessage: String) {
        let eventResult = EventResult(type, sender, value, errorDescription, errorTitle, errorMessage)
        //print("RaiseEvent:\(type):\(String(describing: sender)):\(String(describing: value)):\(String(describing: errorDescription)):\(String(describing: errorTitle)):\(String(describing: errorMessage))")
        RaiseEvent(eventResult)
    }
    public func RaiseEvent(_ eventResult: EventResult) {
        for listener in listeners.enumerated() {
            if (listener.element.type == eventResult.type) {
                let function = listener.element.function
                function(eventResult)
                break
            }
        }
    }
    
    public func RaiseEvents(_ eventResult: EventResult) {
        for listener in listeners.enumerated() {
            let function = listener.element.function
            function(eventResult)
        }
    }
}
