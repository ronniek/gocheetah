import Foundation

public class EventResult {
    // members
    public let type: String
    public let status: EventResultStatusTypeEnum
    public let sender: AnyObject?
    
    public let value: Any?
    public let errorDescription: String
    public let errorTitle: String
    public let errorMessage: String
    
    // class
    init() {
        type = ""
        status = .failed
        sender = nil
        
        value = nil
        errorDescription = ""
        errorTitle = ""
        errorMessage = ""
    }
    
    public init(_ type: String, _ sender: AnyObject?, _ value: Any? = nil) {
        self.type = type
        status = .success
        self.sender = sender
        
        self.value = value
        errorDescription = ""
        errorTitle = ""
        errorMessage = ""
    }
    public init(_ type: String, _ sender: AnyObject?, _ value: Any?, _ errorDescription: String, _ errorTitle: String, _ errorMessage: String) {
        self.type = type
        status = .failed
        self.sender = sender
        
        self.value = value
        self.errorDescription = errorDescription
        self.errorTitle = errorTitle
        self.errorMessage = errorMessage
    }
}
