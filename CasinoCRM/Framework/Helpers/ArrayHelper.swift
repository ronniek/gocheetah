import Foundation

public class ArrayHelper {
    public static func Sum(_ array: [Int]) -> Int {
        var result: Int = 0
        
        for item in array {
            result = result + item
        }
        
        return result
    }
    public static func Sum(_ array: [Decimal]) -> Decimal {
        var result: Decimal = 0
        
        for item in array {
            result = result + item
        }
        
        return result
    }
    public static func Sum(_ array: [Float]) -> Float {
        var result: Float = 0
        
        for item in array {
            result = result + item
        }
        
        return result
    }
}
