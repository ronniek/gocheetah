import Foundation

public class BundleHelper {
    // usage
    public static func getVersionString() -> String {
        return get("CFBundleShortVersionString")
    }
    public static func getServerIsLaptop() -> Bool {
        return get("SERVER_IS_LAPTOP")
    }
    public static func getServerRootUrl() -> String {
        if getServerIsLaptop() {
            return "http://10.100.102.29/SRYCoding.CasinoCRM.WS/Services/WS"
        } else {
            //return "http://actswebappws.azurewebsites.net/Services/WS"
            return "http://www.mocky.io/v2"
        }
    }

    // private
    private static func get(_ key: String) -> [String] {
        return Bundle.main.object(forInfoDictionaryKey: key) as! [String]
    }
    private static func get(_ key: String) -> [String : String] {
        return Bundle.main.object(forInfoDictionaryKey: key) as! [String : String]
    }
    private static func get(_ key: String) -> Bool {
        return Bundle.main.object(forInfoDictionaryKey: key) as! Bool
    }
    private static func get(_ key: String) -> Data {
        return Bundle.main.object(forInfoDictionaryKey: key) as! Data
    }
    private static func get(_ key: String) -> Date {
        return Bundle.main.object(forInfoDictionaryKey: key) as! Date
    }
    private static func get(_ key: String) -> Decimal {
        return Bundle.main.object(forInfoDictionaryKey: key) as! Decimal
    }
    private static func get(_ key: String) -> Float {
        return Bundle.main.object(forInfoDictionaryKey: key) as! Float
    }
    private static func get(_ key: String) -> String {
        return Bundle.main.object(forInfoDictionaryKey: key) as! String
    }
}
