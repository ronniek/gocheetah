import ObjectMapper

public class DateFormatTransform: TransformType {
    public typealias Object = Date
    public typealias JSON = String
    
    public func transformFromJSON(_ value: Any?) -> Object? {
        if let dateString = value as? String {
            return dateString.toDate()?.date
        }
        return nil
    }
    public func transformToJSON(_ value: Date?) -> JSON? {
        if let date = value {
            return date.toString()
        }
        return nil
    }
}
