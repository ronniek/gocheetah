import Foundation

public class SecurityHelper {
    // members
    private static let NUMERIC = "0123456789"
    private static let ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    private static let ALPHA_NUMERIC = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    
    // methods
    public static func GenerateNumericPassword(_ length: Int) -> String {
        return Generate(NUMERIC, length)
    }
    public static func GenerateAlphaPassword(_ length: Int) -> String {
        return Generate(ALPHA, length)
    }
    public static func GenerateAlphaNumericPassword(_ length: Int) -> String {
        return Generate(ALPHA_NUMERIC, length)
    }

    private static func Generate(_ pool: String, _ length: Int) -> String {
        var result = "";
    
        for _ in 1...length {
            result = result + pool.substring(Int.random(in: 0..<pool.count), 1)
        }
    
        return result;
    }
}
