import Foundation

public class FileHelper {
	// file
    public static func copy(_ from: URL, _ to: URL) -> Bool {
        do {
            try FileManager.default.copyItem(at: from, to: to)
            return true
        } catch {
            return false
        }
    }
    
    public static func move(_ from: URL, _ to: URL) -> Bool {
        do {
            try FileManager.default.moveItem(at: from, to: to)
            return true
        } catch {
            return false
        }
    }

    public static func exists(_ path: URL) -> Bool {
        return FileManager.default.fileExists(atPath: path.path)
    }
    
    // path
    public static func fileName(_ path: URL) -> String {
        return path.pathExtension == "" ? path.lastPathComponent : path.lastPathComponent.components(separatedBy: ".")[0]
    }
    public static func fileNameAndExtension(_ path: URL) -> String {
        let fileName = FileHelper.fileName(path)
        let fileNameaAndExtension = path.pathExtension == "" ? fileName : "\(fileName).\(path.pathExtension)"
        return fileNameaAndExtension
    }
    
    public static func concat(_ url: URL, _ paths: String...) -> URL? {
        var result: URL = url
        for path in paths {
            result = result.appendingPathComponent(String(describing: path))
        }
        return result
    }

    public static func remove(_ path: URL) -> Bool {
        do {
        	try FileManager.default.removeItem(at: path)
            return true
        } catch {
            print(error)
            return false
        }
    }
    
    // directory
    public static func createDirectory(_ path: URL) {
        do {
            try FileManager.default.createDirectory(at: path, withIntermediateDirectories: true, attributes: nil)
        } catch {
        }
    }

    public static func getApplicationDirectory() -> URL {
        return getDirectory(.applicationDirectory)!
    }
    public static func getDocumentDirectory() -> URL {
        return getDirectory(.documentDirectory)!
    }
    public static func getLibraryDirectory() -> URL {
        return getDirectory(.libraryDirectory)!
    }
    public static func getTmpDirectory() -> URL {
        if #available(iOS 10.0, *) {
            return FileManager.default.temporaryDirectory
        } else {
            return getLibraryDirectory("tmp")
        }
    }

    public static func getDocumentDirectory(_ paths: String...) -> URL {
        var result = getDocumentDirectory()
        for path in paths {
            result = result.appendingPathComponent(String(describing: path))
        }
        return result
    }
    public static func getLibraryDirectory(_ paths: String...) -> URL {
        var result = getLibraryDirectory()
        for path in paths {
            result = result.appendingPathComponent(String(describing: path))
        }
        return result
    }
    public static func getTmpDirectory(_ paths: String...) -> URL {
        var result = getTmpDirectory()
        for path in paths {
            result = result.appendingPathComponent(String(describing: path))
        }
        return result
    }

    private static func getDirectory(_ directory: FileManager.SearchPathDirectory) -> URL? {
        if let directory = FileManager.default.urls(for: directory, in: .userDomainMask).first {
            return directory
        } else {
            return nil
        }
    }
}
