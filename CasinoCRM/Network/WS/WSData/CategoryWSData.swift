import ObjectMapper

open class CategoryWSData: BaseWSData {
    // members
    public var id: Int
    public var name: String
    public var parent_id: String
    public var description: String
    public var image_url: String
    public var display_order: Int
    public var image_id: String
    public var hide: Bool

    // mapping
    open override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
        parent_id <- map["parent_id"]
        description <- map["description"]
        image_url <- map["image_url"]
        display_order <- map["display_order"]
        image_id <- map["image_id"]
        hide <- map["hide"]
    }
    
    // class
    public override init() {
        self.id = -1
        self.name = ""
        self.parent_id = ""
        self.description = ""
        self.image_url = ""
        self.display_order = -1
        self.image_id = ""
        self.hide = false
        super.init()
   }
    public init(_ id: Int, _ name: String, _ parent_id: String, _ description: String, _ image_url: String, _ display_order: Int, _ image_id: String, _ hide: Bool) {
        self.id = id
        self.name = name
        self.parent_id = parent_id
        self.description = description
        self.image_url = image_url
        self.display_order = display_order
        self.image_id = image_id
        self.hide = hide
        super.init()
    }
    public required init?(map: Map) {
        self.id = 0
        self.name = ""
        self.parent_id = ""
        self.description = ""
        self.image_url = ""
        self.display_order = 0
        self.image_id = ""
        self.hide = false
        super.init(map: map)
    }
}
