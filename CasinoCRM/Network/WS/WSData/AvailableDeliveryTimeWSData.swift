import ObjectMapper

open class AvailableDeliveryTimeWSData: BaseWSData {
    // members
    public var date: Date
    public var start: Int
    public var end: Int
    public var reserved: Bool
    public var spots_left: Int
    public var show_last_spots_left: Bool

    // mapping
    open override func mapping(map: Map) {
        super.mapping(map: map)
        date <- (map["date"], DateFormatTransform())
        start <- map["start"]
        end <- map["end"]
        reserved <- map["reserved"]
        spots_left <- map["spots_left"]
        show_last_spots_left <- map["show_last_spots_left"]
    }
    
    // class
    public override init() {
        self.date = Date()
        self.start = -1
        self.end = -1
        self.reserved = false
        self.spots_left = -1
        self.show_last_spots_left = false
        super.init()
   }
    public init(_ date: Date, _ start: Int, _ end: Int, _ reserved: Bool, _ spots_left: Int, _ show_last_spots_left: Bool) {
        self.date = date
        self.start = start
        self.end = end
        self.reserved = reserved
        self.spots_left = spots_left
        self.show_last_spots_left = show_last_spots_left
        super.init()
    }
    public required init?(map: Map) {
        self.date = Date()
        self.start = 0
        self.end = 0
        self.reserved = false
        self.spots_left = 0
        self.show_last_spots_left = false
        super.init(map: map)
    }
}
