import ObjectMapper

open class CartWSData: BaseWSData {
    // members
    public var id: Int
    public var cart_total: Int
    public var CartTotal: String {
        return NumberHelper.numberAccountingFormat(Decimal(cart_total) / 100)
    }
    public var total: Int
    public var delivery_fee: Int
    public var same_day_charge: Int
    public var created_at_iso8601: Date
    public var last_time_modified_int: Int64
    public var delivery_date_iso8601: Date
    public var status: String
    public var delivery_address: String
    public var promo_code: String
    public var promo_code_discount: Int
    public var promo_code_discount_cash: Int
    public var promo_code_validation: Bool
    public var restaurant_id: Int
    public var note: String
    public var same_day_charge_amount: Int
    public var sub_total: Int
    public var error_code: String
    public var error_description: String
    public var delivery_date: String
    public var delivery_time_start: Int
    public var delivery_time_end: Int
    public var local_time_iso8601: Date
    public var credit_card_charge: Int
    public var first_delivery_discount: Int
    public var free_delivery_discount: Int
    public var payment_method: String
    public var fill_in: Bool
    public var possible_fill_in: String
    public var subscription_fee: Int
    public var under_subscription: String
    public var order_items_information: Array<OrderItemInformationWSData>
    public var ItemsCount: String {
        if order_items_information.count == 0 {
            return "No Items"
        } else {
            return "\(order_items_information.count) items"
        }
    }
    public var available_delivery_times: Array<AvailableDeliveryTimeWSData>

    // mapping
    open override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        cart_total <- map["cart_total"]
        total <- map["total"]
        delivery_fee <- map["delivery_fee"]
        same_day_charge <- map["same_day_charge"]
        created_at_iso8601 <- (map["created_at_iso8601"], DateFormatTransform())
        last_time_modified_int <- map["last_time_modified_int"]
        delivery_date_iso8601 <- (map["delivery_date_iso8601"], DateFormatTransform())
        status <- map["status"]
        delivery_address <- map["delivery_address"]
        promo_code <- map["promo_code"]
        promo_code_discount <- map["promo_code_discount"]
        promo_code_discount_cash <- map["promo_code_discount_cash"]
        promo_code_validation <- map["promo_code_validation"]
        restaurant_id <- map["restaurant_id"]
        note <- map["note"]
        same_day_charge_amount <- map["same_day_charge_amount"]
        sub_total <- map["sub_total"]
        error_code <- map["error_code"]
        error_description <- map["error_description"]
        delivery_date <- map["delivery_date"]
        delivery_time_start <- map["delivery_time_start"]
        delivery_time_end <- map["delivery_time_end"]
        local_time_iso8601 <- (map["local_time_iso8601"], DateFormatTransform())
        credit_card_charge <- map["credit_card_charge"]
        first_delivery_discount <- map["first_delivery_discount"]
        free_delivery_discount <- map["free_delivery_discount"]
        payment_method <- map["payment_method"]
        fill_in <- map["fill_in"]
        possible_fill_in <- map["possible_fill_in"]
        subscription_fee <- map["subscription_fee"]
        under_subscription <- map["under_subscription"]
        order_items_information <- map["order_items_information"]
        available_delivery_times <- map["available_delivery_times"]
    }
    
    // class
    public override init() {
        self.id = -1
        self.cart_total = -1
        self.total = -1
        self.delivery_fee = -1
        self.same_day_charge = -1
        self.created_at_iso8601 = Date()
        self.last_time_modified_int = -1
        self.delivery_date_iso8601 = Date()
        self.status = ""
        self.delivery_address = ""
        self.promo_code = ""
        self.promo_code_discount = -1
        self.promo_code_discount_cash = -1
        self.promo_code_validation = false
        self.restaurant_id = -1
        self.note = ""
        self.same_day_charge_amount = -1
        self.sub_total = -1
        self.error_code = ""
        self.error_description = ""
        self.delivery_date = ""
        self.delivery_time_start = -1
        self.delivery_time_end = -1
        self.local_time_iso8601 = Date()
        self.credit_card_charge = -1
        self.first_delivery_discount = -1
        self.free_delivery_discount = -1
        self.payment_method = ""
        self.fill_in = false
        self.possible_fill_in = ""
        self.subscription_fee = -1
        self.under_subscription = ""
        self.order_items_information = Array<OrderItemInformationWSData>()
        self.available_delivery_times = Array<AvailableDeliveryTimeWSData>()
        super.init()
   }
    public init(_ id: Int, _ cart_total: Int, _ total: Int, _ delivery_fee: Int, _ same_day_charge: Int, _ created_at_iso8601: Date, _ last_time_modified_int: Int64, _ delivery_date_iso8601: Date, _ status: String, _ delivery_address: String, _ promo_code: String, _ promo_code_discount: Int, _ promo_code_discount_cash: Int, _ promo_code_validation: Bool, _ restaurant_id: Int, _ note: String, _ same_day_charge_amount: Int, _ sub_total: Int, _ error_code: String, _ error_description: String, _ delivery_date: String, _ delivery_time_start: Int, _ delivery_time_end: Int, _ local_time_iso8601: Date, _ credit_card_charge: Int, _ first_delivery_discount: Int, _ free_delivery_discount: Int, _ payment_method: String, _ fill_in: Bool, _ possible_fill_in: String, _ subscription_fee: Int, _ under_subscription: String, _ order_items_information: Array<OrderItemInformationWSData>, _ available_delivery_times: Array<AvailableDeliveryTimeWSData>) {
        self.id = id
        self.cart_total = cart_total
        self.total = total
        self.delivery_fee = delivery_fee
        self.same_day_charge = same_day_charge
        self.created_at_iso8601 = created_at_iso8601
        self.last_time_modified_int = last_time_modified_int
        self.delivery_date_iso8601 = delivery_date_iso8601
        self.status = status
        self.delivery_address = delivery_address
        self.promo_code = promo_code
        self.promo_code_discount = promo_code_discount
        self.promo_code_discount_cash = promo_code_discount_cash
        self.promo_code_validation = promo_code_validation
        self.restaurant_id = restaurant_id
        self.note = note
        self.same_day_charge_amount = same_day_charge_amount
        self.sub_total = sub_total
        self.error_code = error_code
        self.error_description = error_description
        self.delivery_date = delivery_date
        self.delivery_time_start = delivery_time_start
        self.delivery_time_end = delivery_time_end
        self.local_time_iso8601 = local_time_iso8601
        self.credit_card_charge = credit_card_charge
        self.first_delivery_discount = first_delivery_discount
        self.free_delivery_discount = free_delivery_discount
        self.payment_method = payment_method
        self.fill_in = fill_in
        self.possible_fill_in = possible_fill_in
        self.subscription_fee = subscription_fee
        self.under_subscription = under_subscription
        self.order_items_information = order_items_information
        self.available_delivery_times = available_delivery_times
        super.init()
    }
    public required init?(map: Map) {
        self.id = 0
        self.cart_total = 0
        self.total = 0
        self.delivery_fee = 0
        self.same_day_charge = 0
        self.created_at_iso8601 = Date()
        self.last_time_modified_int = 0
        self.delivery_date_iso8601 = Date()
        self.status = ""
        self.delivery_address = ""
        self.promo_code = ""
        self.promo_code_discount = 0
        self.promo_code_discount_cash = 0
        self.promo_code_validation = false
        self.restaurant_id = 0
        self.note = ""
        self.same_day_charge_amount = 0
        self.sub_total = 0
        self.error_code = ""
        self.error_description = ""
        self.delivery_date = ""
        self.delivery_time_start = 0
        self.delivery_time_end = 0
        self.local_time_iso8601 = Date()
        self.credit_card_charge = 0
        self.first_delivery_discount = 0
        self.free_delivery_discount = 0
        self.payment_method = ""
        self.fill_in = false
        self.possible_fill_in = ""
        self.subscription_fee = 0
        self.under_subscription = ""
        self.order_items_information = Array<OrderItemInformationWSData>()
        self.available_delivery_times = Array<AvailableDeliveryTimeWSData>()
        super.init(map: map)
    }
}
