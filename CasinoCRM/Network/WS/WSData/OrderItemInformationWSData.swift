import ObjectMapper

open class OrderItemInformationWSData: BaseWSData {
    // members
    public var id: Int
    public var quantity: Int
    public var product_id: Int
    public var sub_total: Int
    public var SubTotal: String {
        return NumberHelper.numberAccountingFormat(Decimal(sub_total) / 100)
    }
    public var order_id: Int
    public var packaging_type: String
    public var substitutable: Bool
    public var product: ProductWSData
    public var PorductURL: String {
        switch packaging_type {
        case "unit":
            return product.unit_photo_filename
        case "case":
            return product.pack_photo_file
        case "weight":
            return product.weight_photo_filename
        default:
            return ""
        }
    }

    // mapping
    open override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        quantity <- map["quantity"]
        product_id <- map["product_id"]
        sub_total <- map["sub_total"]
        order_id <- map["order_id"]
        packaging_type <- map["packaging_type"]
        substitutable <- map["substitutable"]
        product <- map["product"]
    }
    
    // class
    public override init() {
        self.id = -1
        self.quantity = -1
        self.product_id = -1
        self.sub_total = -1
        self.order_id = -1
        self.packaging_type = ""
        self.substitutable = false
        self.product = ProductWSData()
        super.init()
   }
    public init(_ id: Int, _ quantity: Int, _ product_id: Int, _ sub_total: Int, _ order_id: Int, _ packaging_type: String, _ substitutable: Bool, _ product: ProductWSData) {
        self.id = id
        self.quantity = quantity
        self.product_id = product_id
        self.sub_total = sub_total
        self.order_id = order_id
        self.packaging_type = packaging_type
        self.substitutable = substitutable
        self.product = product
        super.init()
    }
    public required init?(map: Map) {
        self.id = 0
        self.quantity = 0
        self.product_id = 0
        self.sub_total = 0
        self.order_id = 0
        self.packaging_type = ""
        self.substitutable = false
        self.product = ProductWSData()
        super.init(map: map)
    }
}
