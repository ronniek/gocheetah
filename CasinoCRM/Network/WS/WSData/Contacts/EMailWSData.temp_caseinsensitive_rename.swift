import ObjectMapper

open class EMailWSData: BaseWSData {
    // members
    public var EMailID: String
    public var EMailType: String
    public var Description: String
    public var EMail: String
    public var AllowEMailPromotion: Bool
    public var IsDefault: Bool
    
    // mapping
    open override func mapping(map: Map) {
        super.mapping(map: map)
        EMailID <- map["EMailID"]
        EMailType <- map["EMailType"]
        Description <- map["Description"]
        EMail <- map["EMail"]
        AllowEMailPromotion <- map["AllowEMailPromotion"]
        IsDefault <- map["IsDefault"]
    }
    
    // class
    public override init() {
        self.EMailID = ""
        self.EMailType = ""
        self.Description = ""
        self.EMail = ""
        self.AllowEMailPromotion = false
        self.IsDefault = false
        super.init()
    }
    public required init?(map: Map) {
        self.EMailID = ""
        self.EMailType = ""
        self.Description = ""
        self.EMail = ""
        self.AllowEMailPromotion = false
        self.IsDefault = false
        super.init(map: map)
    }

    // methods
    override open func toString() -> String {
        return "{0}".format(EMail as AnyObject)
    }
}
