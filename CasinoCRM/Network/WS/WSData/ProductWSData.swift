import ObjectMapper

open class ProductWSData: BaseWSData {
    // members
    public var id: Int
    public var name: String
    public var sku: String
    public var unit_photo_filename: String
    public var pack_photo_file: String
    public var unit_photo_hq_url: String
    public var pack_photo_hq_url: String
    public var unit_barcode: String
    public var pack_barcode: String
    public var available: Bool
    public var updated_at_iso8601: Date
    public var case_orderable: Bool
    public var weight_orderable: Bool
    public var weight_photo_filename: String
    public var weight_photo_hq_url: String
    public var unit_price: Int
    public var UnitPrice: String {
        return NumberHelper.numberAccountingFormat(Decimal(unit_price) / 100)
    }
    public var case_price: Int
    public var weight_price: Int
    public var items_per_unit: Int
    public var units_per_case: Int
    public var avg_unit_weight: Int
    public var avg_case_weight: Int
    public var weight_discount_price: Int
    public var weight_discount_threshold: Int
    public var grouped_by_category_name: String
    public var categories: Array<CategoryWSData>

    // mapping
    open override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
        sku <- map["sku"]
        unit_photo_filename <- map["unit_photo_filename"]
        pack_photo_file <- map["pack_photo_file"]
        unit_photo_hq_url <- map["unit_photo_hq_url"]
        pack_photo_hq_url <- map["pack_photo_hq_url"]
        unit_barcode <- map["unit_barcode"]
        pack_barcode <- map["pack_barcode"]
        available <- map["available"]
        updated_at_iso8601 <- (map["updated_at_iso8601"], DateFormatTransform())
        case_orderable <- map["case_orderable"]
        weight_orderable <- map["weight_orderable"]
        weight_photo_filename <- map["weight_photo_filename"]
        weight_photo_hq_url <- map["weight_photo_hq_url"]
        unit_price <- map["unit_price"]
        case_price <- map["case_price"]
        weight_price <- map["weight_price"]
        items_per_unit <- map["items_per_unit"]
        units_per_case <- map["units_per_case"]
        avg_unit_weight <- map["avg_unit_weight"]
        avg_case_weight <- map["avg_case_weight"]
        weight_discount_price <- map["weight_discount_price"]
        weight_discount_threshold <- map["weight_discount_threshold"]
        grouped_by_category_name <- map["grouped_by_category_name"]
        categories <- map["categories"]
    }
    
    // class
    public override init() {
        self.id = -1
        self.name = ""
        self.sku = ""
        self.unit_photo_filename = ""
        self.pack_photo_file = ""
        self.unit_photo_hq_url = ""
        self.pack_photo_hq_url = ""
        self.unit_barcode = ""
        self.pack_barcode = ""
        self.available = false
        self.updated_at_iso8601 = Date()
        self.case_orderable = false
        self.weight_orderable = false
        self.weight_photo_filename = ""
        self.weight_photo_hq_url = ""
        self.unit_price = -1
        self.case_price = -1
        self.weight_price = -1
        self.items_per_unit = -1
        self.units_per_case = -1
        self.avg_unit_weight = -1
        self.avg_case_weight = -1
        self.weight_discount_price = -1
        self.weight_discount_threshold = -1
        self.grouped_by_category_name = ""
        self.categories = Array<CategoryWSData>()
        super.init()
   }
    public init(_ id: Int, _ name: String, _ sku: String, _ unit_photo_filename: String, _ pack_photo_file: String, _ unit_photo_hq_url: String, _ pack_photo_hq_url: String, _ unit_barcode: String, _ pack_barcode: String, _ available: Bool, _ updated_at_iso8601: Date, _ case_orderable: Bool, _ weight_orderable: Bool, _ weight_photo_filename: String, _ weight_photo_hq_url: String, _ unit_price: Int, _ case_price: Int, _ weight_price: Int, _ items_per_unit: Int, _ units_per_case: Int, _ avg_unit_weight: Int, _ avg_case_weight: Int, _ weight_discount_price: Int, _ weight_discount_threshold: Int, _ grouped_by_category_name: String, _ categories: Array<CategoryWSData>) {
        self.id = id
        self.name = name
        self.sku = sku
        self.unit_photo_filename = unit_photo_filename
        self.pack_photo_file = pack_photo_file
        self.unit_photo_hq_url = unit_photo_hq_url
        self.pack_photo_hq_url = pack_photo_hq_url
        self.unit_barcode = unit_barcode
        self.pack_barcode = pack_barcode
        self.available = available
        self.updated_at_iso8601 = updated_at_iso8601
        self.case_orderable = case_orderable
        self.weight_orderable = weight_orderable
        self.weight_photo_filename = weight_photo_filename
        self.weight_photo_hq_url = weight_photo_hq_url
        self.unit_price = unit_price
        self.case_price = case_price
        self.weight_price = weight_price
        self.items_per_unit = items_per_unit
        self.units_per_case = units_per_case
        self.avg_unit_weight = avg_unit_weight
        self.avg_case_weight = avg_case_weight
        self.weight_discount_price = weight_discount_price
        self.weight_discount_threshold = weight_discount_threshold
        self.grouped_by_category_name = grouped_by_category_name
        self.categories = categories
        super.init()
    }
    public required init?(map: Map) {
        self.id = 0
        self.name = ""
        self.sku = ""
        self.unit_photo_filename = ""
        self.pack_photo_file = ""
        self.unit_photo_hq_url = ""
        self.pack_photo_hq_url = ""
        self.unit_barcode = ""
        self.pack_barcode = ""
        self.available = false
        self.updated_at_iso8601 = Date()
        self.case_orderable = false
        self.weight_orderable = false
        self.weight_photo_filename = ""
        self.weight_photo_hq_url = ""
        self.unit_price = 0
        self.case_price = 0
        self.weight_price = 0
        self.items_per_unit = 0
        self.units_per_case = 0
        self.avg_unit_weight = 0
        self.avg_case_weight = 0
        self.weight_discount_price = 0
        self.weight_discount_threshold = 0
        self.grouped_by_category_name = ""
        self.categories = Array<CategoryWSData>()
        super.init(map: map)
    }
}
