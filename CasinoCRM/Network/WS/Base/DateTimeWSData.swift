import ObjectMapper

open class DateTimeWSData: BaseWSData {
    // members
    public var Value: Date
    
    // mapping
    open override func mapping(map: Map) {
        super.mapping(map: map)
        Value <- (map["ValueString"], DateFormatTransform())
    }
    
    // class
    public override init() {
        self.Value = Date()
        super.init()
    }
    public init(_ date: Date) {
        self.Value = date
        super.init()
    }
    public required init?(map: Map) {
        self.Value = Date()
        super.init(map: map)
    }
}
