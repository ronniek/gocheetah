import ObjectMapper

open class BaseListWSData<T>: BaseWSData {
    // members
    public var List: Array<T>
    
    // mapping
    open override func mapping(map: Map) {
        super.mapping(map: map)
        List <- map["List"]
    }
    
    // class
    public override init() {
        self.List = Array<T>()
        super.init()
    }
    public init(_ list: Array<T>) {
        self.List = list
        super.init()
    }
    public required init?(map: Map) {
        self.List = Array()
        super.init(map: map)
    }
}
