import Alamofire
import AlamofireObjectMapper

open class BaseWSAdapter<T: BaseWSData> : EventDispatcher {
    // members
    public var isSuccess: Bool
    public var title: String
    public var text: String
    public var wsResult: WSResult<T>?
    public var wsResult2: T?
    public var WSData: T {
        return wsResult2!
    }
    public var json: String
    
    open var wsPath: String {
        return "override to set ws path"
    }
    
    open var httpMethod: HTTPMethod {
        return HTTPMethod.get
    }
    
    open var httpHeaders: HTTPHeaders {
        let httpHeaders = HTTPHeaders()
        //httpHeaders["Accept"] = ContentTypeEnum.ApplicationJson.rawValue
        return httpHeaders
    }
    
    open var httpParameters = [String: Any]()
    
    open var encoding: ParameterEncoding = URLEncoding.queryString
    
    open var random: Bool {
        return false
    }
    
    // offline
    public var offlineWSResult: WSResult<T>? {
        return nil
    }
    
    // class
    public override init() {
        isSuccess = false
        title = ""
        text = ""
        wsResult = nil
        json = ""
    }
    
    // methods
    public func addParams(_ key:String, _ value: String) {
        httpParameters[key] = value
    }
    public func addParams(_ key:String, _ value: Int) {
        httpParameters[key] = value
    }
    public func addParams(_ key:String, _ value: UInt32) {
        httpParameters[key] = value
    }
    public func addParams(_ key:String, _ value: Int64) {
        httpParameters[key] = value
    }
    public func addParams(_ key:String, _ value: Double) {
        httpParameters[key] = value
    }
    public func addParams(_ key:String, _ value: Date) {
        httpParameters[key] = DateTimeHelper.dateToYYYYMMDD_HHMMSS(value)
    }
    public func addParams(_ key:String, _ value: Bool) {
        if value {
            addParams(key, "true")
        } else {
            addParams(key, "false")
        }
    }
    
    // execute
    public func executeTest() {
        addParams("test", Consts.TEST_TOKEN)
        executeAdapter()
    }
    public func execute() {
        addParams("test", "")
        executeAdapter()
    }
    private func executeAdapter() {
        let url: URL = FileHelper.concat(URL(string: BundleHelper.getServerRootUrl())!, wsPath)!
        
        if random {
            addParams("random", arc4random_uniform(999999999))
        }

        let configuration = URLSessionConfiguration.default
        configuration.httpMaximumConnectionsPerHost = 50

        Alamofire
            //.SessionManager(configuration: configuration)
            .request(url, method: httpMethod, parameters: httpParameters, encoding: encoding, headers: httpHeaders)
            .validate(statusCode: 200..<300)
            .responseObject { (response: DataResponse<T>) in
                if let wsResult2: T = response.result.value {
                    self.wsResult2 = wsResult2
                    
                    if (self.wsResult2 == nil) {
                        self.isSuccess = false
                        self.title = "Incorrect WS Data"
                        self.text = "Response data = nil.\nPlease contact the administrator"

                        self.postExecute(false)
                    } else {
                        self.isSuccess = true

                        self.postExecute(true)
                    }
                } else {
                    self.postExecute(false)
                }
        }

        /*
        #if targetEnvironment(simulator)
            Alamofire
                //.SessionManager(configuration: configuration)
                .request(url, method: httpMethod, parameters: httpParameters, encoding: encoding, headers: httpHeaders)
                .validate(statusCode: 200..<300)
                .responseObject { (response: DataResponse<WSResult<T>>) in
                    if let wsResult: WSResult<T> = response.result.value {
                        self.wsResult = wsResult
                        self.json = wsResult.json
                        
                        if (self.wsResult?.data == nil) {
                            self.isSuccess = false
                            self.title = "Incorrect WS Data"
                            self.text = "Response data = nil.\nPlease contact the administrator"
                            
                            self.postExecute(false)
                        } else {
                            self.isSuccess = wsResult.isSuccess
                            self.title = wsResult.title
                            self.text = wsResult.text
                            
                            self.postExecute(wsResult.isSuccess)
                        }
                    } else {
                        self.postExecute(false)
                    }
            }
        #else
            Alamofire
                //.SessionManager(configuration: configuration)
                .request(url, method: httpMethod, parameters: httpParameters, encoding: encoding, headers: httpHeaders)
                .validate(statusCode: 200..<300)
                .responseObject { (response: DataResponse<WSResult<T>>) in
                    if let wsResult: WSResult<T> = response.result.value {
                        self.wsResult = wsResult
                        self.json = wsResult.json
                        
                        if (self.wsResult?.data == nil) {
                            self.isSuccess = false
                            self.title = "Incorrect WS Data"
                            self.text = "Response data = nil.\nPlease contact the administrator"
                            
                            self.postExecute(false)
                        } else {
                            self.isSuccess = wsResult.isSuccess
                            self.title = wsResult.title
                            self.text = wsResult.text
                            
                            self.postExecute(wsResult.isSuccess)
                        }
                    } else {
                        self.postExecute(false)
                    }
            }
        #endif
        */
    }
    public func postExecute(_ isSuccess: Bool) {
        if isSuccess {
            let result = testResponseData()
            if result == "" {
                self.RaiseEvent(self.CompletedEvent, self)
            } else {
                title = "Incorrect WS Data"
                text = result
                self.RaiseEvent(self.ErrorEvent, self)
            }
        } else {
            if title == "" {
                title = "Error"
            }
            if text == "" {
                text = "Server error. Please contact the administrator"
            }
            self.RaiseEvent(self.ErrorEvent, self)
        }
    }
    
    // test WS response data
    private func testResponseData() -> String {
        if (self.json == "") {
            return ""
        } else {
            var dicPre: [String: Any] = [:]
            if let data = self.json.data(using: .utf8) {
                do {
                    dicPre = try JSONSerialization.jsonObject(with: data, options: []) as! [String : Any]
                } catch {
                    return error.localizedDescription
                }
            }
            let dicPost : [String: Any] = (self.wsResult?.data?.toDictionary())!
            
            let strPrePost = compareDicPrePost("Swift", dicPre, dicPost)
            let strPostPre = compareDicPrePost("C#", dicPost, dicPre)
            
            if strPrePost == "" && strPostPre == "" {
                return ""
            } else if strPostPre != "" && strPrePost == "" {
                return strPostPre
            } else if strPostPre == "" && strPrePost != "" {
                return strPrePost
            } else {
                return strPrePost
            }
        }
    }
    private func compareDicPrePost(_ check: String, _ dic1: [String: Any], _ dic2: [String: Any]) -> String {
        for (key, _) in dic1 {
            switch key {
            case "CompletedEvent":
                continue
            case "listeners":
                continue
            case "ErrorEvent":
                continue
            default:
                let o = dic2[key]
                if o == nil {
                    return "\(key) not found in \(check)"
                }
            }
        }
        
        return ""
    }
}
