import ObjectMapper

public class WSResult<T: BaseWSData>: BaseMappableDispatcher {
    // members
    public var isSuccess: Bool
    public var status: String
    public var title: String
    public var text: String
    public var data: T?
    public var json: String
    
    // mapping
    public override func mapping(map: Map) {
        super.mapping(map: map)
        isSuccess <- map["IsSuccess"]
        status <- map["Status"]
        title <- map["Title"]
        text <- map["Text"]
        data <- map["Data"]
        json <- map["Json"]
    }
    
    // class
    public init(_ isSuccess: Bool, _ status: String, _ title: String, _ text: String, _ data: T?, _ json: String) {
        self.isSuccess = isSuccess
        self.status = status
        self.title = title
        self.text = text
        self.data = data
        self.json = json
        super.init()
    }
    public required init?(map: Map) {
        self.isSuccess = false
        self.status = ""
        self.title = ""
        self.text = ""
        self.data = nil
        self.json = ""
        super.init(map: map)
    }
}
